import java.lang.*;
import java.util.*;
import java.io.*;
class Main{
	public static HashMap<String, Integer>marks_sheet = new HashMap<String, Integer>();
	public static void readTextFile(String file) {
		Scanner sc = new Scanner(new File(file));
		while(sc.hasNext()){
			String str = sc.nextLine();
			string []line = str.split("\\s+");
			marks_sheet.put(line[0],line[1]);
		}
	}
	public static void main(String []args){
		readTextFile("home/shifu/WE2019/resources/marklist.txt");
	    List<Map.Entry<String, Integer> > list = new LinkedList<Map.Entry<String, Integer> >(marks_sheet.entrySet()); 
	    Collections.sort(list, new Comparator<Map.Entry<String, Integer> >() { 
	            public int compare(Map.Entry<String, Integer> p1,  
	                               Map.Entry<String, Integer> p2) { 
	                return (p2.getValue()).compareTo(p1.getValue()); 
	            } 
	    }); 
	    int rank = 1;
	    for (Map.Entry<String, Integer> marks : list) { 
	       System.out.print(rank+" "+marks.getKey()+" "+marks.getValue()); 
	       rank = rank + 1;
	   }   
	} 
}
